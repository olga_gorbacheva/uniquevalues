package ru.god.uniquevalues;

import java.util.HashSet;

/**
 * Класс, реализующий отбор уникальных значений
 *
 * @author Горбачева, 16ИТ18к
 */
public class UniqueValues {
    public static void main(String[] args) {
        int[] array = {11, 2, 3, 7, 4, 10, 2, 6, 8, 4, 3, 9, 5, 10};
        HashSet<Integer> uniqueValues = new HashSet<>();
        for (int value : array) {
            uniqueValues.add(value);
        }
        System.out.println(uniqueValues);
    }
}
